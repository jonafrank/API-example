const gulp = require('gulp');
const gls = require('gulp-live-server');
const apidoc = require('gulp-apidoc');

const server = gls.new('src/index.js');

gulp.task('apidoc', (done) => {
    apidoc({
        src: 'src/routes',
        dest: 'src/apidoc',
    }, done);
});

gulp.task('serve', gulp.series('apidoc', () => {
    server.start();
}));

gulp.task('serve:dev', gulp.series('apidoc', () => {
    server.start();
    gulp.watch([
        './src/index.js',
        './src/config/**/*.js',
        './src/controllers/**/*.js',
        './src/middlewares/**/*.js',
        './src/models/**/*.js',
        './src/routes/**/*.js',
    ], gulp.parallel('serve'));
}));
