const express = require('express');
const validate = require('express-validation');
const validationSchema = require('../middlewares/param-validation');
const users = require('../controllers/users');

const router = express.Router();

/**
 * @api {post} /users Create a new User
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiName CreateUser
 * @apiDescription Creates a new user in the system.
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam (body) {String}   name   User's name.
 * @apiParam (body) {String}   avatar User's avatar, valid URL.
 * @apiParamExample {json} Request-Example:
 *  {
 *    "name": "John Doe"
 *    "avatar": "http://samplesite.com/jon_doe.jpeg"
 *  }
 *
 * @apiSuccess (201) {String} _id    New user unique ID.
 * @apiSuccess (201) {String} name   New user name.
 * @apiSuccess (201) {String} avatar New user avatar url.
 * @apiSuccessExample {json} Success-Response:
 *  HTTP/1.1 201 Created
 *  {
 *    "_id": "5b626875093b9e00112ea21e",
 *    "name": "John Doe",
 *    "avatar": "http://samplesite.com/jon_doe.jpeg"
 *  }
 * @apiUse UnauthorizedError
 * @apiUse ForbiddenError
 */
router.post('/users', validate(validationSchema.createUser), users.createUser);

module.exports = router;
