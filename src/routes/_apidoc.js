// ------------------------------------------------------------------------------------------
// General apiDoc documentation blocks and old history blocks.
// ------------------------------------------------------------------------------------------

/**
 * @apiDefine UnauthorizedError
 * @apiVersion 1.0.0
 * @apiError Unauthorized the Header <code>Authorization</code> wasn't sent.
 * @apiErrorExample {json} Unauthorized-Response:
 *  HTTP/1.1 401 Unauthorized.
 *  {
 *    "status": 401,
 *    "statusText": "Unauthorized",
 *    "errors": [
 *        {
 *            "message": "Missing \"Authorization\" header."
 *        }
 *    ]
 *  }
 */

/**
 * @apiDefine ForbiddenError
 * @apiVersion 1.0.0
 * @apiError Forbidden Invalid token.
 * @apiErrorExample {json} Forbidden-Response:
 *     HTTP/1.1 403 Forbidden
 *     {
 *       "status": 403,
 *       "statusText": "Forbidden",
 *       "errors": [
 *           {
 *               "message": "You ar not allowed to access this resource URI."
 *           }
 *       ]
 *     }
 */

/**
 * @apiDefine AuthorizationHeader
 * @apiVersion 1.0.0
 * @apiHeader {String} Authorization Bearer Token for authorization
 * @apiHeaderExample {String} Header-Example:
 *     Authorization: Bearer 5CD4ED173E1C95FE763B753A297D5
 */
