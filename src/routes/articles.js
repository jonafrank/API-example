const express = require('express');
const validate = require('express-validation');
const validationSchema = require('../middlewares/param-validation');
const articles = require('../controllers/articles');

const router = express.Router();

/**
 * @api {post} /users/:userId/articles Creates a new Article
 * @apiVersion 1.0.0
 * @apiGroup Articles
 * @apiName CreateArticle
 * @apiDescription Creates a new Article in the system.
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam (body) {String}   title Article's title.
 * @apiParam (body) {String}   text  Article's body Text.
 * @apiParam (body) {String[]} tags  Article's text tags.
 * @apiParamExample {json} Request-Example:
 *  {
 *    "title": "New Article Title",
 *    "text": "Lorem Ipsum ...",
 *    "tags": ["lorem", "ipsum"]
 *  }
 * @apiSuccess (201) {String}   _id           New Article unique ID.
 * @apiSuccess (201) {String}   title         New Article title.
 * @apiSuccess (201) {String}   text          New Article body text.
 * @apiSuccess (201) {String[]} tags          New Article text tags.
 * @apiSuccess (201) {Object}   author        Article's Author information.
 * @apiSuccess (201) {String}   author._id    Article's author user ID.
 * @apiSuccess (201) {String}   author.name   Article's author name.
 * @apiSuccess (201) {String}   author.avatar Article's author avatar url.
 * @apiSuccessExample {json} Success-Respoonse
 *  HTTP/1.1 201 Created
 * {
 *    "_id": "5b6def5f07fe61001c247174",
 *    "title": "New Article Title",
 *    "text": "Lorem Ipsum ...",
 *    "tags": [
 *        "lorem",
 *        "ipsum"
 *    ],
 *    "author": {
 *        "_id": "5b626875093b9e00112ea21e",
 *        "name": "John Doe",
 *        "avatar": "hhttp://samplesite.com/jon_doe.jpeg"
 *    }
 * }
 * @apiUse UnauthorizedError
 * @apiUse ForbiddenError
 */
router.post('/users/:userId/articles', validate(validationSchema.createArticle), articles.createArticle);

/**
 * @api {put} /articles/:articleId Edit Article
 * @apiVersion 1.0.0
 * @apiGroup Articles
 * @apiName EditAticle
 * @apiDescription Updates an Article in the system.
 * @apiUse AuthorizationHeader
 *
 * @apiParam (body) {String}   title Article's title.
 * @apiParam (body) {String}   text  Article's body Text.
 * @apiParam (body) {String[]} tags  Article's text tags.
 * @apiParamExample {json} Request-Example:
 *  {
 *    "title": "Edited Article Title",
 *    "text": " Edited Lorem Ipsum ...",
 *    "tags": ["lorem", "ipsum", "edited"]
 *  }
 * @apiSuccess {String}   _id           New Article unique ID.
 * @apiSuccess {String}   title         New Article title.
 * @apiSuccess {String}   text          New Article body text.
 * @apiSuccess {String[]} tags          New Article text tags.
 * @apiSuccess {Object}   author        Article's Author information.
 * @apiSuccess {String}   author._id    Article's author user ID.
 * @apiSuccess {String}   author.name   Article's author name.
 * @apiSuccess {String}   author.avatar Article's author avatar url.
 * @apiSuccessExample {json} Success-Respoonse
 *  HTTP/1.1 200 Success
 * {
 *    "_id": "5b6def5f07fe61001c247174",
 *    "title": "Edited Article Title",
 *    "text": "Edited Lorem Ipsum ...",
 *    "tags": [
 *        "lorem",
 *        "ipsum",
 *        "edited"
 *    ],
 *    "author": {
 *        "_id": "5b626875093b9e00112ea21e",
 *        "name": "John Doe",
 *        "avatar": "hhttp://samplesite.com/jon_doe.jpeg"
 *    }
 * }
 * @apiUse UnauthorizedError
 * @apiUse ForbiddenError
 */

router.put('/articles/:articleId', validate(validationSchema.editArticle), articles.editArticle);

/**
 * @api {delete} /articles/:articleId Delete Article
 * @apiVersion 1.0.0
 * @apiGroup Articles
 * @apiName DeleteArticle
 * @apiDescription Delets an Article in the system.
 * @apiUse AuthorizationHeader
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 204 No Contetnt
 * @apiUse UnauthorizedError
 * @apiUse ForbiddenError
 */
router.delete('/articles/:articleId', articles.deleteArticle);

/**
 * @api {get} /articles/:articleId Get Articles
 * @apiVersion 1.0.0
 * @apiGroup Articles
 * @apiName GetArticles
 * @apiDescription Return a paginated response of all articles.
 * @apiUse AuthorizationHeader
 *
 * @apiParam (query) {String[]} tags  A list of tags to search.
 * @apiParam (query) {Number}   page  The number of the page to get.
 * @apiParam (query) {Number}   limit Limit of records per page.
 *
 * @apiSuccess {Object}   pager               Pagination data.
 * @apiSuccess {Number}   pager.pages         Total number of pages.
 * @apiSuccess {Number}   pager.page          Current page.
 * @apiSuccess {Number}   pager.limit         Records per page.
 * @apiSuccess {String}   pager.next          Next Page URL.
 * @apiSuccess {String}   pager.prev          Previous Page URL.
 * @apiSuccess {Object[]} items               Result data.
 * @apiSuccess {String}   items._id           Article unique ID.
 * @apiSuccess {String}   items.title         Article title.
 * @apiSuccess {String}   items.text          Article body text.
 * @apiSuccess {String[]} items.tags          Article text tags.
 * @apiSuccess {Object}   items.author        Article's Author information.
 * @apiSuccess {String}   items.author._id    Article's author user ID.
 * @apiSuccess {String}   items.author.name   Article's author name.
 * @apiSuccess {String}   items.author.avatar Article's author avatar url.
 * @apiSuccessExample {json} Success-Response:
 *  HTTP/1.1 200 Success
 *  {
 *     "pager": {
 *       "pages": 3,
 *       "page": 2,
 *       "limit": 5,
 *       "next": "http://localhost:3042/v1/articles?limit=5&page=3",
 *       "prev": "http://localhost:3042/v1/articles?limit=5&page=1"
 *     },
 *     "items": [
 *       ...
 *       {
 *          "_id": "5b6def5f07fe61001c247174",
 *          "title": "Edited Article Title",
 *          "text": "Edited Lorem Ipsum ...",
 *          "tags": [
 *              "lorem",
 *              "ipsum",
 *              "edited"
 *          ],
 *          "author": {
 *              "_id": "5b626875093b9e00112ea21e",
 *              "name": "John Doe",
 *              "avatar": "hhttp://samplesite.com/jon_doe.jpeg"
 *          }
 *       }
 *       ...
 *     ]
 *  }
 * @apiUse UnauthorizedError
 * @apiUse ForbiddenError
 */
router.get('/articles', articles.getArticles);


module.exports = router;
