const express = require('express');
const userRoutes = require('./users');
const articleRoutes = require('./articles');

const router = express.Router();

router.use('/v1', userRoutes);
router.use('/v1', articleRoutes);

module.exports = router;
