const express = require('express');
const mongoose = require('mongoose');
const http = require('http');
const bodyParser = require('body-parser');
const unless = require('express-unless');
const path = require('path');
const config = require('./config');
const routes = require('./routes');
const isAuthenticated = require('./middlewares/is-authenticated');

isAuthenticated.unless = unless;
const app = express();

// connect to mongo db
const mongoServer = config.database.server;
const mongoPort = config.database.port;
const mongoDatabase = config.database.name;
try {
    mongoose.connect(`mongodb://${mongoServer}:${mongoPort}/${mongoDatabase}`, { useNewUrlParser: true });
} catch (er) {
    console.log('Waiting to DB be Ready ...');
    setTimeout(() => {
        mongoose.connect(`mongodb://${mongoServer}:${mongoPort}/${mongoDatabase}`, { useNewUrlParser: true });
    }, 3000);
}

app.use(bodyParser.json({
    limit: '1mb',
}));

app.use('/apidoc', express.static(path.join(__dirname, 'apidoc')));
// Set up authentication

app.use('/', routes);

// error handler, required as of 0.3.0
app.use((err, req, res, next) => { // eslint-disable-line
    res.status(400).json(err);
});

const { runningPort } = config.app;
const server = http.createServer(app);
server.listen(runningPort, () => {
    console.info(`Blog API Example running on ${config.app.baseUrl}`);
});

module.exports = server;
