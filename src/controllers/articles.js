const _ = require('lodash');
const Article = require('../models/Article');
const User = require('../models/User');
const config = require('../config');

const { notFoundResponse } = require('../helpers/error-responses');
const { buildQueryString } = require('../helpers/utils');

const createArticle = async (req, res) => {
    const user = await User.findById(req.params.userId);
    if (!user) {
        return res.status(404).json(notFoundResponse('User with the given "userId" not found'));
    }
    const article = new Article({
        user: user._id,
        title: req.body.title,
        text: req.body.text,
        tags: req.body.tags,
    });
    await article.save();
    return res.status(201).json({
        _id: article.id,
        title: article.title,
        text: article.text,
        tags: article.tags || [],
        author: {
            _id: user._id,
            name: user.name,
            avatar: user.avatar,
        },
    });
};

const editArticle = async (req, res) => {
    const article = await Article.findById(req.params.articleId)
        .populate('user');
    if (!article) {
        return res.status(404).json(notFoundResponse('Article with the given "articleId" not found.'));
    }

    article.title = req.body.title || article.title;
    article.text = req.body.text || article.text;
    article.tags = req.body.tags || article.tags;
    await article.save();
    return res.status(200).json({
        _id: article.id,
        title: article.title,
        text: article.text,
        tags: article.tags,
        author: {
            _id: article.user.id,
            name: article.user.name,
            avatar: article.user.name,
        },
    });
};

const deleteArticle = async (req, res) => {
    const article = await Article.findById(req.params.articleId);
    if (!article) {
        return res.status(404).json(notFoundResponse('Article with the given "articleId" not found.'));
    }
    await article.delete();
    return res.status(204);
};

const getArticles = async (req, res) => {
    const query = {};
    if (typeof req.query.tags !== 'undefined') {
        if (Array.isArray(req.query.tags)) {
            query.tags = { $elemMatch: { $in: req.query.tags } };
        } else {
            query.tags = req.query.tags;
        }
    }
    const pagerOptions = {
        page: Number(req.query.page) || 1,
        limit: Number(req.query.limit) || 20,
    };
    const result = await Article.paginate(query, pagerOptions);
    await User.populate(result.docs, { path: 'user' });
    const nextQuery = Object.assign({}, req.query);
    const prevQuery = Object.assign({}, req.query);
    nextQuery.page = result.page + 1;
    prevQuery.page = result.page - 1;

    // build Items.
    const items = [];
    _.forEach(result.docs, (val) => {
        items.push({
            _id: val._id,
            title: val.title,
            text: val.text,
            tags: val.tags,
            author: {
                _id: val.user._id,
                name: val.user.name,
                avatar: val.user.avatar,
            },
        });
    });

    return res.status(200).json({
        pager: {
            pages: result.pages,
            page: result.page,
            limit: result.limit,
            next: (result.page < result.pages) ? `${config.app.baseUrl}/v1/articles?${buildQueryString(nextQuery)}` : null,
            prev: (result.page > 1) ? `${config.app.baseUrl}/v1/articles?${buildQueryString(prevQuery)}` : null,
        },
        items,
    });
};

module.exports = {
    createArticle,
    editArticle,
    deleteArticle,
    getArticles,
};
