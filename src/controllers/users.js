const User = require('../models/User');

const createUser = async (req, res) => {
    const user = new User({
        name: req.body.name,
        avatar: req.body.avatar,
    });
    await user.save();
    return res.status(201).json({
        _id: user._id,
        name: user.name,
        avatar: user.avatar,
    });
};

module.exports = {
    createUser,
};
