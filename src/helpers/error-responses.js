const notFoundResponse = message => ({
    status: 404,
    statusText: 'Not Found',
    errors: [
        {
            message,
        },
    ],
});

module.exports = {
    notFoundResponse,
};
