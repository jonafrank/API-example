
function buildQueryString(obj) {
    return Object.keys(obj).map(key => `${key}=${encodeURIComponent(obj[key])}`).join('&');
}

module.exports = {
    buildQueryString,
};
