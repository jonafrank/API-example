const config = require('../config');

const isAuthenticated = (req, res, next) => {
    console.log(req);
    const authHeader = req.get('Authorization');
    if (!authHeader) {
        return res.status(401).json({
            status: 401,
            statusText: 'Unauthorized',
            errors: [
                { message: 'Missing "Authorization" header.' },
            ],
        });
    }
    const token = authHeader.replace('Bearer ', '');
    if (token !== config.authorization.token) {
        return res.status(403).json({
            status: 403,
            statusText: 'Forbidden',
            errors: [
                { message: 'You ar not allowed to access this resource URI.' },
            ],
        });
    }
    return next();
};

module.exports = isAuthenticated;
