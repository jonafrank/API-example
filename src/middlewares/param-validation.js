const Joi = require('joi');

const validationSchemas = {
    createUser: {
        body: {
            name: Joi.string().required(),
            avatar: Joi.string().uri().required(),
        },
    },
    createArticle: {
        body: {
            title: Joi.string().required(),
            text: Joi.string().required(),
            tags: Joi.array().items(Joi.string()),
        },
    },
    editArticle: {
        body: {
            title: Joi.string(),
            text: Joi.string(),
            tags: Joi.array().items(Joi.string()),
        },
    },
};

module.exports = validationSchemas;
