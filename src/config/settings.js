const app = {
    runningPort: process.env.PORT || 3042,
    serverName: process.env.HOST,
};
app.baseUrl = process.env.BASE_URL || `http://${app.serverName}:${app.runningPort}`;

const database = {
    server: process.env.DB_HOST,
    port: process.env.DB_PORT,
    name: process.env.DB_NAME,
};

const authorization = {
    token: process.env.AUTH_TOKEN,
};

module.exports = {
    app,
    database,
    authorization,
};
