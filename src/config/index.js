const settings = require('./settings.js');

module.exports = {
    app: settings.app,
    database: settings.database,
    authorization: settings.authorization,
};
