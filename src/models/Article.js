const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const { Schema } = mongoose;

const articleSchema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    title: { type: String },
    text: { type: String },
    tags: [{ type: String }],
});
articleSchema.plugin(mongoosePaginate);

const Article = mongoose.model('Article', articleSchema);

module.exports = Article;
