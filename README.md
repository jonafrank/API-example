# API-example

## Requirements
* [Docker](https://www.docker.com/)
* [docker-compose](https://docs.docker.com/compose/install/)

## Start API server
* Clone this repository.
* In the root directory run `docker-compose up`.
 > **Note:** If you are using linux probably you will need to run above command with `sudo`

 * By default the server will be running in http://localhost:3042

 ## API Documentation

To see the detailed API documentation you can go to http://localhost:3042/apidoc

### Authorization

All API endpoints are secured so in order to test it you will need to add an `Authorization` Header.
By Default: 
```
Authorization: Bearer 5CD4ED173E1C95FE763B753A297D5
```

You can change this editing the ENV variable in the `docker-compose.yml` file.

## API Tests
In order to test the API you can change `DB_NAME` env variable in `docker-compose.yml` file and re run the server.

After that run `docker-compose exec api mocha /tests` from the root of the proyect.



