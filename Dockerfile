FROM node:10

WORKDIR /usr/src/app

COPY package*.json  ./

RUN npm install
RUN npm install -g gulp
RUN npm install -g gulp-cli
RUN npm install -g apidoc

COPY . .

EXPOSE 3042

CMD ["gulp", "serve:dev"]