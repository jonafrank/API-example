/* eslint func-names: "off", prefer-arrow-callback: "off" */
const chai = require('chai');
const chaiHttp = require('chai-http');
const config = require('../src/config');

const should = chai.should(); // eslint-disable-line
chai.use(chaiHttp);

describe('Article Endpoints', function () {
    let user;
    let article;
    before(function (done) {
        chai.request(config.app.baseUrl)
            .post('/v1/users')
            .set('Authorization', `Bearer ${config.authorization.token}`)
            .send({
                name: 'Cosme Fulanito',
                avatar: 'http://validurl.com/avatar.jpeg',
            })
            .end((err, res) => {
                user = res.body;
                done();
            });
    });

    describe('/POST Article', function () {
        it('Should create a New article', function (done) {
            chai.request(config.app.baseUrl)
                .post(`/v1/users/${user._id}/articles`)
                .set('Authorization', `Bearer ${config.authorization.token}`)
                .send({
                    title: 'Test Title for Article',
                    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nisl ante, tristique ac enim eget, malesuada tincidunt magna. Proin dignissim odio diam, id molestie libero mattis vitae. Suspendisse sollicitudin fringilla diam a luctus. Morbi tellus sem, lobortis non lacus euismod, pulvinar dignissim urna. Sed sit amet turpis eros. Suspendisse bibendum condimentum urna, sit amet dapibus neque malesuada et. Nunc non augue dui.',
                    tags: ['new', 'article', 'lorem'],
                })
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('_id');
                    res.body.should.have.property('title');
                    res.body.should.have.property('text');
                    res.body.should.have.property('tags');
                    res.body.should.have.property('author');
                    article = res.body;
                    done();
                });
        });

        it('Should Return a Not Found Response', function (done) {
            chai.request(config.app.baseUrl)
                .post('/v1/users/123456abcdef/articles')
                .set('Authorization', `Bearer ${config.authorization.token}`)
                .send({
                    title: 'Test Title for Article',
                    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nisl ante, tristique ac enim eget, malesuada tincidunt magna. Proin dignissim odio diam, id molestie libero mattis vitae. Suspendisse sollicitudin fringilla diam a luctus. Morbi tellus sem, lobortis non lacus euismod, pulvinar dignissim urna. Sed sit amet turpis eros. Suspendisse bibendum condimentum urna, sit amet dapibus neque malesuada et. Nunc non augue dui.',
                    tags: ['new', 'article', 'lorem'],
                })
                .end((err, res) => {
                    res.should.have.status(404);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status');
                    res.body.should.have.property('statusText');
                    res.body.should.have.property('errors');
                    res.body.status.should.equal(404);
                    done();
                });
        });
    });

    describe('/PUT articles', function () {
        it('Should Update an article', function (done) {
            chai.request(config.app.baseUrl)
                .put(`/v1/articles/${article._id}`)
                .set('Authorization', `Bearer ${config.authorization.token}`)
                .send({
                    title: 'New Test Title for Article',
                    tags: ['new', 'article', 'lorem', 'New Tag'],
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('title');
                    res.body.should.have.property('text');
                    res.body.should.have.property('tags').with.lengthOf(4);
                    res.body.should.have.property('author');
                    res.body.title.should.eq('New Test Title for Article');
                    done();
                });
        });
    });

    describe('/GET articles', function () {
        it('Should return a paginated result of articles', function (done) {
            chai.request(config.app.baseUrl)
                .get('/v1/articles')
                .set('Authorization', `Bearer ${config.authorization.token}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('pager');
                    res.body.should.have.property('items');
                    done();
                });
        });
    });
});
