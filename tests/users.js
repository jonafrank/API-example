/* eslint func-names: "off", prefer-arrow-callback: "off" */
const chai = require('chai');
const chaiHttp = require('chai-http');
const config = require('../src/config');

const should = chai.should(); // eslint-disable-line
chai.use(chaiHttp);
describe('Users Endpoints', function () {
    describe('/POST users', function () {
        it('Should Create a new User', function (done) {
            chai.request(config.app.baseUrl)
                .post('/v1/users')
                .set('Authorization', `Bearer ${config.authorization.token}`)
                .send({
                    name: 'Cosme Fulanito',
                    avatar: 'http://validurl.com/avatar.jpeg',
                })
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('_id');
                    res.body.should.have.property('name');
                    res.body.should.have.property('avatar');
                    res.body.name.should.equal('Cosme Fulanito');
                    res.body.avatar.should.equal('http://validurl.com/avatar.jpeg');
                    done();
                });
        });

        it('Should Get an Unauthorized Error', function (done) {
            chai.request(config.app.baseUrl)
                .post('/v1/users')
                .send({
                    name: 'Cosme Fulanito',
                    avatar: 'http://validurl.com/avatar.jpeg',
                })
                .end((err, res) => {
                    res.should.have.status(401);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status');
                    res.body.should.have.property('statusText');
                    res.body.should.have.property('errors');
                    res.body.status.should.equal(401);
                    done();
                });
        });

        it('Should Get Forbidden Error', function (done) {
            chai.request(config.app.baseUrl)
                .post('/v1/users')
                .set('Authorization', 'Bearer 12345ABCDEF')
                .send({
                    name: 'Cosme Fulanito',
                    avatar: 'http://validurl.com/avatar.jpeg',
                })
                .end((err, res) => {
                    res.should.have.status(403);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status');
                    res.body.should.have.property('statusText');
                    res.body.should.have.property('errors');
                    res.body.status.should.equal(403);
                    done();
                });
        });
    });
});
